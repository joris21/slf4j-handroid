package org.slf4j.impl.spi;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author Martin Vysny <mavi@vaadin.com>
 */
public class TestLogger implements SpiLogger {
    public final List<String> messages = new ArrayList<>();

    @Override
    public void println(int priority, String name, String message, Throwable throwable) {
        messages.add(message);
    }

    public void assertMessages(String... messages) {
        assertArrayEquals(messages, this.messages.toArray());
    }

    @Override
    public String toString() {
        return "TestLogger{" +
                "messages=" + messages +
                '}';
    }
}
