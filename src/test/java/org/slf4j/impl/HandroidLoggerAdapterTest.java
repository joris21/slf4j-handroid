package org.slf4j.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.slf4j.impl.HandroidLoggerAdapter.postprocessMessage;

/**
 * @author Martin Vysny <mavi@vaadin.com>
 */
public class HandroidLoggerAdapterTest {
    @Test
    public void testWhitespaceCharRemoval() {
        assertEquals("Foo     bar", postprocessMessage("Foo\r\2\3\4\5bar"));
    }

    @Test
    public void testRemoveConsecutiveNewlines() {
        assertEquals("\nFoo\nbar\n", postprocessMessage("\n\nFoo\n\n\n\nbar\n"));
    }
}
