package org.slf4j.impl.spi;

import android.util.Log;
import org.jetbrains.annotations.NotNull;

/**
 * Service Point Implementation logger. A simple interface which ultimately calls Android logger.
 */
public interface SpiLogger {
    /**
     * Logs stuff to Android logger.
     * @param priority e.g. {@link Log#ERROR}
     * @param name the logger name, must be properly truncated if need be. Not null.
     * @param message the message. Not null.
     * @param throwable optional throwable for reference. The message already contains the stacktrace of this throwable. May be null.
     */
    void println(int priority, String name, String message, Throwable throwable);

    /**
     * Simply calls {@link Log#println(int, String, String)}.
     */
    SpiLogger BASIC = new SpiLogger() {
        @Override
        public void println(int priority, String name, String message, Throwable throwable) {
            Log.println(priority, name, message);
        }
    };

    enum Type {
        BASIC {
            @NotNull
            @Override
            public SpiLogger createLogger() {
                return SpiLogger.BASIC;
            }
        },
        FABRIC_CRASHLYTICS {
            @NotNull
            @Override
            public SpiLogger createLogger() {
                return new CrashlyticsSpiLogger();
            }
        },
        FIREBASE_CRASHLYTICS {
            @NotNull
            @Override
            public SpiLogger createLogger() {
                return new FirebaseCrashlyticsSpiLogger();
            }
        };

        @NotNull
        public abstract SpiLogger createLogger();
    }
}
