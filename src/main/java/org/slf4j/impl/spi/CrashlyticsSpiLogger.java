package org.slf4j.impl.spi;

import android.util.Log;

import java.lang.reflect.Method;

/**
 * Logs to Crashlytics. Since Crashlytics internally calls Log.println(), there
 * is no need to use {@link SpiLogger#BASIC}.
 */
public class CrashlyticsSpiLogger implements SpiLogger {
    private static final Method crashlyticsLog;
    private static final Method crashlyticsLogException;
    static {
        try {
            final Class<?> crashlyticsClass = Class.forName("com.crashlytics.android.Crashlytics");
            // yes I know, reflection is slower. Yet crashlytics doesn't seem to provide jars, only aar which I can't link against.
            crashlyticsLog = crashlyticsClass.getDeclaredMethod("log", int.class, String.class, String.class);
            crashlyticsLogException = crashlyticsClass.getDeclaredMethod("logException", Throwable.class);
            System.out.println("slf4j-handroid: enabling integration with Crashlytics");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void println(int priority, String name, String message, Throwable throwable) {
        // this also internally calls Log.println(), so no need to do it ourselves
        try {
            crashlyticsLog.invoke(null, priority, name, message);
            if (priority >= Log.WARN && throwable != null) {
                crashlyticsLogException.invoke(null, throwable);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
