package org.slf4j.impl.spi;

/**
 * A {@link SpiLogger} that breaks up log messages into multiple logs no longer than {@link #maxLength}.
 * See https://gitlab.com/mvysny/slf4j-handroid/-/issues/8 for more details.
 */
public class MessageLengthLimitingLogger implements SpiLogger {
    /**
     * max length allowed for a log message
     */
    private final int maxLength;
    /**
     * if log message is longer than {@link #maxLength}, attempt to break the log
     * message at a new line between [minLength] and [maxLength] if one exists
     */
    private final int minLength;
    private final SpiLogger delegate;

    public MessageLengthLimitingLogger(SpiLogger delegate) {
        this(4000, 3000, delegate);
    }

    public MessageLengthLimitingLogger(int maxLength, int minLength, SpiLogger delegate) {
        if (maxLength <= 0) {
            throw new IllegalArgumentException("Parameter maxLength: invalid value " + maxLength + ": must be 1 or greater");
        }
        if (minLength <= 0) {
            throw new IllegalArgumentException("Parameter minLength: invalid value " + minLength + ": must be 1 or greater");
        }
        if (delegate == null) {
            throw new IllegalArgumentException("Parameter delegate: invalid value " + delegate + ": must not be null");
        }
        if (maxLength < minLength) {
            throw new IllegalArgumentException("Parameter maxLength: invalid value " + maxLength + ": must be greater than " + minLength);
        }
        this.maxLength = maxLength;
        this.minLength = minLength;
        this.delegate = delegate;
    }

    @Override
    public void println(int priority, String name, String message, Throwable throwable) {
        // String to be logged is longer than the max...
        if (message.length() > maxLength) {
            String msgSubstring = message.substring(0, maxLength);
            int msgSubstringEndIndex = maxLength;

            // Try to find a substring break at a newline char.
            int lastIndex = msgSubstring.lastIndexOf('\n');
            if (lastIndex >= minLength) {
                msgSubstring = msgSubstring.substring(0, lastIndex);
                // skip over new line char
                msgSubstringEndIndex = lastIndex + 1;
            }

            // Log the substring.
            delegate.println(priority, name, msgSubstring, throwable);

            // Recursively log the remainder.
            println(priority, name, message.substring(msgSubstringEndIndex), null);
        } else {
            // String to be logged is shorter than the max...
            delegate.println(priority, name, message, throwable);
        }
    }
}
