package org.slf4j.impl.spi;

import android.util.Log;

import java.lang.reflect.Method;

/**
 * Logs to Firebase Crashlytic and to Android Log.
 */
public class FirebaseCrashlyticsSpiLogger implements SpiLogger {
    private static final Object firebaseCrashlytics;
    private static final Method logMethod;
    private static final Method recordExceptionMethod;
    static {
        try {
            final Class<?> crashlyticsClass = Class.forName("com.google.firebase.crashlytics.FirebaseCrashlytics");
            // yes I know, reflection is slower. Yet crashlytics doesn't seem to provide jars, only aar which I can't link against.
            Method instanceStaticMethod = crashlyticsClass.getDeclaredMethod("getInstance");
            firebaseCrashlytics = instanceStaticMethod.invoke(null);
            logMethod = crashlyticsClass.getMethod("log", String.class);
            recordExceptionMethod = crashlyticsClass.getMethod("recordException", Throwable.class);
            System.out.println("slf4j-handroid: enabling integration with Firebase Crashlytics");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void println(int priority, String name, String message, Throwable throwable) {
        Log.println(priority, name, message);
        try {
            logMethod.invoke(firebaseCrashlytics, "[" + name + "] " + message);
            if (priority >= Log.WARN && throwable != null) {
                recordExceptionMethod.invoke(firebaseCrashlytics, throwable);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
